﻿#pragma strict

// private var ChatScripUrl= "http://enurai.encs.concordia.ca/chatbot/chatscriptclient.php?";
// private var NewIDUrl= "http://enurai.encs.concordia.ca/chatbot/chatscriptid.php?";
//private var ChatScripUrl= "http://127.0.0.1/chatscriptclient.php?";
//private var NewIDUrl= "http://127.0.0.1/chatscriptid.php?";
private var ChatScripUrl= "http://167.160.163.209/lablablab/chatscriptclient.php?";
private var NewIDUrl= "http://167.160.163.209/lablablab/chatscriptid.php?";
private var userID = "globule";
private var userNumber:int = 0;
private var userInput : String = "[Type your answer here]";
private var userText : String = "Hello young lady";
private var botOutput : String = null;
private var consoleText : String;
private var scrollPosition : Vector2 = Vector2.zero;
private var showLog : boolean = false;
private var hasWon:boolean = false;
//private var keyboard: TouchScreenKeyboard;
// Text bubble variables

public var textDelay = 0.2;
private var botWords : String = "Testing, testing, one two, one two";
private var botCurrentWords : String = "";
private var playerWords : String = "Testing, testing, one two, one two";
private var playerCurrentWords : String = "";

// Flags

private var botTalking : boolean = false;
private var playerTalking : boolean = false;


// GUI

var inputBoxCoords : Rect;
var inputBoxStyle : GUIStyle;
var logBoxStyle : GUIStyle;
var logCoords:Rect;
var logTextStyle:GUIStyle;
var textBubbleStyle : GUIStyle;
var textBubbleStyle2 : GUIStyle;
var textBubbleCoords : Rect;
var userBubbleCoords : Rect;
var buttonStyle : GUIStyle;
var restartButtonCoords : Rect;
var logButtonCoords : Rect;
var trustLabelCoords : Rect;
var patienceLabelCoords : Rect;
var labelStyle : GUIStyle;

public var TrustBar : ProgressBar;
public var PatienceBar : ProgressBar;
public var Fader:GUITexture;

//Sounds
public var ambienceSound:AudioClip;
public var sfxError:AudioClip[]; //Array to make a random bank of sound variations. Use with AudioSource.PlayClipAtPoint(sfxError[Random.Range(0, sfxError.Length)], transform.position);
public var sfxAction:AudioClip;
public var sfxTrust:AudioClip[];



//Game variables
var session : int = 0;
var trust : int = 0;
var maxTrust : int = 5;
var patience : int = 6;
var maxPatience : int = 6;

// Game objects
var BlancheNeige : Transform;

// private var timeStamp : System.DateTime;
// private var currentTime : String = null;

private var waitingForMessage: boolean = false;
private var waitingForId: boolean = false;

function Start ()
{
	getNewID ();
	initConversation ();
}


function botSays (bubbletext : String)
{

    bubbletext = bubbletext.Replace ("\r", "");
    bubbletext = bubbletext.Replace ("\n", "");
    bubbletext = parseCodes (bubbletext);
	while (playerTalking == true || botTalking == true || waitingForMessage == true)
	{
		yield WaitForSeconds (0.1);
	}
	
    botTalking = true;

    botWords = bubbletext;
	botCurrentWords = "";
	
//update log:
	consoleText = consoleText+"\n\n[Snow White]: "+botWords;
	scrollPosition.y = Mathf.Infinity; 	
	

    for (var letter in bubbletext.ToCharArray ())
    {
        if (botCurrentWords == bubbletext) break;
        if (botWords != bubbletext) break;
        botCurrentWords += letter;
        yield WaitForSeconds (textDelay * Random.Range (0.25, 0.5)); // Original Random.Range(0.5, 2)
    }
    
    botTalking = false;
    
    if(hasWon){
    
	    for(var i:int=0;i<20;i++){
	    	yield(WaitForSeconds(0.1));
	    }
    	Fader.GetComponent(SceneFadeOut).StartFade("win");
    	
    	
    }

}

function playerSays (bubbletext:String)
{

	while (playerTalking == true || botTalking == true)
	{
		yield WaitForSeconds (0.1);
	}
    playerTalking = true;
    bubbletext = bubbletext.Replace ("\r", "");
    bubbletext = bubbletext.Replace ("\n", "");
    bubbletext = parseCodes (bubbletext);
    playerWords = bubbletext;
	playerCurrentWords = "";
	

    for (var letter in bubbletext.ToCharArray ())
    {

        if (playerCurrentWords == bubbletext) break;
        if (playerWords != bubbletext) break;
        playerCurrentWords += letter;
        yield WaitForSeconds (textDelay * Random.Range (0.01, 0.5)); // Original Random.Range(0.5, 2)
    } 
    playerTalking = false;

}
  
function parseCodes(parseText:String) : String
{

// trust ++
	if (parseText.Contains ("CTplus"))
	{
		trust ++;
		if(trust>maxTrust){trust=maxTrust;}
		parseText = parseText.Replace ("CTplus", "");		
		TrustBar.GetComponent (ProgressBar).changeState (trust, maxTrust);
		BlancheNeige.GetComponent (Animator).SetInteger ("trust", trust);
		AudioSource.PlayClipAtPoint(sfxTrust[Random.Range(0, sfxTrust.Length)], transform.position);
	}
// trust --
	if (parseText.Contains ("CTminus"))
	{
		trust --;
		if(trust<0){trust=0;}
		parseText = parseText.Replace ("CTminus", "");		
		TrustBar.GetComponent (ProgressBar).changeState (trust, maxTrust);
		BlancheNeige.GetComponent (Animator).SetInteger ("trust", trust);
		AudioSource.PlayClipAtPoint(sfxError[Random.Range(0, sfxError.Length)], transform.position);
	}	
// end
	if (parseText.Contains ("CFail"))
	{
		trust = -1;
		parseText = parseText.Replace ("CFail", "");		
		TrustBar.GetComponent (ProgressBar).changeState (0, maxTrust);
		BlancheNeige.GetComponent (Animator).SetInteger ("trust", trust);
		AudioSource.PlayClipAtPoint(sfxError[Random.Range(0, sfxError.Length)], transform.position);
		//print ("fail");
	}	
// end
	if (parseText.Contains ("CPatience"))
	{
		patience --;
		parseText = parseText.Replace ("CPatience", "");		
		PatienceBar.GetComponent (ProgressBar).changeState (patience, maxPatience);
		AudioSource.PlayClipAtPoint(sfxError[Random.Range(0, sfxError.Length)], transform.position);
	}		
// win
	if (parseText.Contains ("CWin"))
	{
		parseText = parseText.Replace ("CWin", "");	
		hasWon = true;
	}	
// gambit
	if (parseText.Contains ("CGambit"))
	{
		parseText = parseText.Replace ("CGambit", "");	
		//playerWords = playerCurrentWords + "- [Snow White curtly interrupts you]";
		//playerCurrentWords = playerWords;
		//print ("CGambit:" + playerCurrentWords);
		// playerSays (playerWords);
	}
	
	return parseText;

}
 


function OnGUI ()
{

	GUI.enabled = true;
	if (!(playerTalking == true || botTalking == true ||  waitingForMessage == true)&&(Event.current.type == EventType.KeyDown && (Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.Return)))
	
	{
		AudioSource.PlayClipAtPoint(sfxAction, transform.position);
		consoleText = consoleText + "\n\n[Player]: " + userInput;

        postMessage (userInput);
        playerSays (userInput);
        userInput = "";
	}


	
	GUI.skin.label.alignment = TextAnchor.MiddleCenter; // Text alignment
	
	// 	User text bubble		
    var textwidth : float = userBubbleCoords.width * Screen.width / 1280;
    var bubbleheight : float = textBubbleStyle2.CalcHeight (GUIContent (playerWords), textwidth);
    var minWidth : float;
    var maxWidth : float;
    textBubbleStyle.CalcMinMaxWidth (GUIContent (playerWords), minWidth,  maxWidth);
    if (maxWidth > textwidth)
    {
    	maxWidth = textwidth;
    }  
    if (maxWidth < 100)
    	maxWidth = 100; 
    GUI.color.a = playerCurrentWords.Length;
    GUI.color.a /= 8;  
    if (playerCurrentWords == playerWords)
    	GUI.color.a = 1;
    GUI.Box (Rect (userBubbleCoords.x * Screen.width / 1280 + textwidth - maxWidth, userBubbleCoords.y * Screen.height / 800, maxWidth, bubbleheight), playerCurrentWords, textBubbleStyle2);
    GUI.color.a = 1.0;
    
    // Bot text bubble
    
    textwidth = textBubbleCoords.width * Screen.width / 1280;
    bubbleheight = textBubbleStyle.CalcHeight (GUIContent (botWords), textwidth);
	textBubbleStyle.CalcMinMaxWidth (GUIContent (botWords), minWidth,  maxWidth);
    if (maxWidth > textwidth)
    {
    	maxWidth = textwidth;
    }    
    if (maxWidth < 100)
    	maxWidth = 100;
    // print (botCurrentWords.Length);
    GUI.color.a = botCurrentWords.Length;
    GUI.color.a /= 8;
    if (botCurrentWords == botWords)
    	GUI.color.a = 1;
    GUI.Box (Rect (textBubbleCoords.x * Screen.width / 1280, textBubbleCoords.y * Screen.height / 800, maxWidth, bubbleheight), botCurrentWords, textBubbleStyle);
    GUI.color.a = 1.0;
    
    if (showLog) // Manages log window elements
	{
		GUILayout.BeginArea(Rect(logCoords.x*Screen.width/1280,logCoords.y*Screen.height/800,logCoords.width*Screen.width/1280,logCoords.height*Screen.height/800),logBoxStyle);
			scrollPosition = GUILayout.BeginScrollView(scrollPosition);
				GUILayout.TextArea(consoleText, logTextStyle);	
			GUILayout.EndScrollView();
		GUILayout.EndArea();	
					
	}

	


		

	
	//Restart Button
	if (GUI.Button (Rect (restartButtonCoords.x * Screen.width / 1280, restartButtonCoords.y * Screen.height / 800, restartButtonCoords.width * Screen.width / 1280, restartButtonCoords.height * Screen.height / 800 ), "[RESTART]", buttonStyle))
	{
		initConversation ();
		AudioSource.PlayClipAtPoint(sfxAction, transform.position);
		// Application.LoadLevel(0); // Application.LoadLevel("test"); // Alternatively, replace the scene's index number with the name of the scene
	}

	//Log Button
	if (GUI.Button (Rect (logButtonCoords.x * Screen.width / 1280, logButtonCoords.y * Screen.height / 800, logButtonCoords.width * Screen.width / 1280, logButtonCoords.height * Screen.height / 800 ), "[LOG]", buttonStyle))
	{
		showLog = !showLog; // Toggles log visibility
		AudioSource.PlayClipAtPoint(sfxAction, transform.position);

	}	
			
	// Progress Bars
	GUI.SetNextControlName ("anything");
	GUI.Label (Rect (trustLabelCoords.x * Screen.width / 1280, trustLabelCoords.y * Screen.height / 800, trustLabelCoords.width * Screen.width / 1280, trustLabelCoords.height * Screen.height / 800 ), "TRUST", labelStyle);
	GUI.Label (Rect (patienceLabelCoords.x * Screen.width / 1280, patienceLabelCoords.y * Screen.height / 800, patienceLabelCoords.width * Screen.width / 1280, patienceLabelCoords.height*Screen.height / 800 ), "PATIENCE", labelStyle);

	// Text box for user input
	if ((Event.current.type == EventType.MouseUp)&& (GUI.GetNameOfFocusedControl() == "inputbox") && (userInput == "[Type your answer here]"))
	{
			userInput = "";
			//keyboard = TouchScreenKeyboard.Open(userInput, TouchScreenKeyboardType.Default);
	}		
	GUI.SetNextControlName ("inputbox");
	
	// Disables the text field while Snow White is talking
	// Disables the text field while Ambar is talking
	if(playerTalking == true || botTalking == true ||  waitingForMessage == true){
		GUI.enabled = false;
	};

	userInput = GUI.TextField (Rect(inputBoxCoords.x*Screen.width/1280,inputBoxCoords.y*Screen.height/800,inputBoxCoords.width*Screen.width/1280,inputBoxCoords.height*Screen.height/800), userInput, inputBoxStyle);

	GUI.enabled = true;


}

function initConversation ()
{
	
	while (userNumber == 0)
	{
		yield WaitForSeconds (0.1);
	}
	
	session++;
	userID = "id_" + userNumber + "_session_" + session;
	print (userID);
	trust = 0;
	patience = 6;
	BlancheNeige.GetComponent (Animator).SetInteger ("trust", trust); // Resets Snow White's animation to the initial state
	playerSays ("Hello young lady.");
	postMessage ("");
	PatienceBar.GetComponent (ProgressBar).changeState (patience, maxPatience);
	TrustBar.GetComponent (ProgressBar).changeState (trust, maxTrust);
}

function getNewID ()
{
    var w = WWW (NewIDUrl);
    waitingForId = true;
    yield w;
    waitingForId = false;
    if (!String.IsNullOrEmpty (w.error))
    {
       botOutput = "I have a problem communicating with my brain, please try talking to me later.";
       botSays (botOutput);
    }
    else 
    {
    	userNumber = int.Parse (w.text); // Retrieve bot response for display in text bubble
	}    
    print ("userNumber:" + userNumber);
}

/*function getTrust () 
{

    var msgURL = ChatScripUrl + "message=" + WWW.EscapeURL ("simon say give me variable " + variable) + "&userID=" + WWW.EscapeURL (userID);
    print (msgURL);
    var w = WWW (msgURL);
    yield w;
    return w.text;
}*/




function postMessage (message : String)
{

    //message = message.Replace (":", "");	// avoid users sending commands to server
    if(message==""){message="bob";}
    var msgURL = ChatScripUrl + "message=" + WWW.EscapeURL (message) + "&userID=" + WWW.EscapeURL (userID);
    print (msgURL);
    var w = WWW (msgURL);
    waitingForMessage = true;
    while(w.isDone!=true){
    	yield WaitForSeconds(0.1);
    }
    waitingForMessage = false;    
    if (!String.IsNullOrEmpty (w.error))
    {
       botOutput = "Sorry but I can't connect to my brain right now, please try talking to me later.";
       botSays (botOutput);
    }
    else 
    {
    	botOutput = w.text; // Retrieve bot response for display in text bubble
		botSays (botOutput);
	}
    
     /* // Recording time stamps for bot replies
    timeStamp = System.DateTime.Now;
	currentTime = String.Format("{0:D2}:{1:D2}:{2:D2}", timeStamp.Hour, timeStamp.Minute, timeStamp.Second);
		
	consoleText = consoleText+"\n["+currentTime+"] Snow White said: "+w.text;
	*/
}